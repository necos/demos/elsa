import requests
import os, shutil, tarfile
import docker
import time


class SliceExperiment:
      def __init__(self, exp_name, tenant_host, tenant_port, slice_desc, home_dir='/home/uceeftu'):
          self.exp_name = exp_name
          self.tenant_host = tenant_host
          self.tenant_port = str(tenant_port)
          self.slice_desc = open(slice_desc, 'r')
          self.__setup_env()
          self.home_dir = home_dir #cannot use os home env when using sudo
          self.slice_orch_stats = self.home_dir + '/NECOS/slice_orchestrator/stats/'
          self.service_orch_stats = '/opt/escape/log/stats/'
          self.service_adaptation_stats = self.home_dir + '/log/stats/'
 
      def __setup_env(self):
          if not os.path.exists(self.exp_name):
             os.makedirs(self.exp_name)
 
 
      def __copy_slice_timings(self):
          print 'Retrieving Slice Creation / Deletion Timings'
          shutil.copyfile(self.slice_orch_stats + 'stats-create-' + self.slice_id, self.exp_name + '/slice-create-' + self.slice_id + '.stat')
          shutil.copyfile(self.slice_orch_stats + 'stats-delete-' + self.slice_id, self.exp_name + '/slice-delete-' + self.slice_id + '.stat')

 
      def __copy_service_orchestration_timings(self, service_exp_name):
          print 'Retrieving Service Orchestration Timings: ' + service_exp_name
          orchestrator_name = 'orch-' + self.slice_id
          client = docker.from_env()
          orchestrator = client.containers.get(orchestrator_name)

          tar_name = self.exp_name + '/service-orch-' + service_exp_name + '.tar'
          f = open(tar_name, 'w')

          found = False
          while not found:
             try:
                bits, stats = orchestrator.get_archive(self.service_orch_stats + service_exp_name + '.stat')
                found = True
             except:
                time.sleep(5)
          
          for chunk in bits:
              f.write(chunk)
 
          f.close()
          f = open(tar_name, 'r')

          tar = tarfile.TarFile(fileobj=f)
          extract_path = './' + self.exp_name + '/'
          tar.extractall(path=extract_path)
          tar.close()
          f.close()

          os.rename(extract_path + service_exp_name + '.stat', extract_path + '/service-orch-' + service_exp_name + '.stat')          
          os.remove(tar_name)

 
      def __copy_service_adaptation_timings(self, service_exp_name, parts):
          print 'Retrieving Service Adaptation Timings: ' + service_exp_name
          for part in parts:
              print 'Looking for part: ' + part
              found = False
              while not found:
                 try:
                    shutil.move(self.service_adaptation_stats + 'edit-config-' + service_exp_name + '-' + part + '.stat', self.exp_name + '/service-adapt-' + service_exp_name + '-' + part + '.stat')
                    found = True
                 except IOError as e:
                    time.sleep(5)


      def create_slice(self):
          print 'Creating Slice'
          url = 'http://' + self.tenant_host + ':' + self.tenant_port + '/tenant/slice'
          r = requests.post(url, data=self.slice_desc, headers={'Content-Type': 'text/x-yaml'}).json()
          print r
          self.slice_id = r['slice_id']
          self.orch_url = r['service_orchestrator']
          self.slice_parts = r['slice_parts']

     
      def delete_slice(self):
          print 'Deleting Slice'
          url = 'http://' + self.tenant_host + ':' + self.tenant_port + '/tenant/slice/'  + self.slice_id
          r = requests.delete(url).json()
          print r
          self.__copy_slice_timings()
 

      def submit_service(self, service_exp_name, service_desc):
          operation = 'create-' + service_exp_name + '-' + self.slice_id
          print 'Submitting Service: ' + operation
          url = self.orch_url + 'edit-config?message-id=' + operation
          service_desc_xml = open(service_desc, 'r')
          r = requests.post(url, data=service_desc_xml, headers={'Content-Type': 'application/xml'})
          self.__copy_service_orchestration_timings(operation)
          self.__copy_service_adaptation_timings(operation, self.slice_parts)
          print r


      def delete_service(self, service_exp_name, service_desc):
          operation = 'delete-' + service_exp_name + '-' + self.slice_id
          print 'Submitting Service: ' + operation
          url = self.orch_url + 'edit-config?message-id=' + operation
          service_desc_xml = open(service_desc, 'r')
          r = requests.post(url, data=service_desc_xml, headers={'Content-Type': 'application/xml'})
          self.__copy_service_orchestration_timings(operation)
          self.__copy_service_adaptation_timings(operation, self.slice_parts)
          print r


      def get_current_runs_num(self):
          filenames = os.listdir(self.exp_name)
          runs = 0
          for filename in filenames:
              if 'slice-delete' in filename:
                 runs += 1
          return runs

   

 
if __name__=='__main__':

   total_runs = 30
   tenant_addr = 'localhost'
   tenant_port = 5001

   slice_desc_path = 'slice_descriptors/'
   service_desc_path = 'service_descriptors/'

   experiments =  [
                   {'name': '01', 'address': tenant_addr, 'port': tenant_port, 'slice_desc': 'slice_info_1.yml',
                    'services': [{'name': '10-nf', 'desc': '1-10_1567427959592.xml'},
                                 {'name': '90-nf', 'desc': '1-90_1567177197932.xml'},
                                 {'name': '180-nf', 'desc': '1-180_1567605482056.xml'}
                                ]
                   },
                   {'name': '02', 'address': tenant_addr, 'port': tenant_port, 'slice_desc': 'slice_info_2x1.yml',
                    'services': [{'name': '10-nf', 'desc': '1-10_1567427959592.xml'},
                                 {'name': '90-nf', 'desc': '1-90_1567177197932.xml'},
                                 {'name': '180-nf', 'desc': '1-180_1567605482056.xml'}
                                ]
                   },
                   {'name': '03', 'address': tenant_addr, 'port': tenant_port, 'slice_desc': 'slice_info_3x1.yml',
                    'services': [{'name': '10-nf', 'desc': '1-10_1567427959592.xml'},
                                 {'name': '90-nf', 'desc': '1-90_1567177197932.xml'},
                                 {'name': '180-nf', 'desc': '1-180_1567605482056.xml'},
                                 {'name': '270-nf', 'desc': '1-270_1567605491436.xml'}
                                ]
                   },
                   {'name': '06', 'address': tenant_addr, 'port': tenant_port, 'slice_desc': 'slice_info_3x2.yml',
                    'services': [{'name': '10-nf', 'desc': '1-10_1567427959592.xml'},
                                 {'name': '90-nf', 'desc': '1-90_1567177197932.xml'},
                                 {'name': '180-nf', 'desc': '1-180_1567605482056.xml'},
                                 {'name': '360-nf', 'desc': '1-360_1567605514736.xml'},
                                 {'name': '540-nf', 'desc': '1-540_1567605552161.xml'}
                                ]
                   },
                   {'name': '08', 'address': tenant_addr, 'port': tenant_port, 'slice_desc': 'slice_info_8.yml',
                    'services': [{'name': '10-nf', 'desc': '1-10_1567427959592.xml'},
                                 {'name': '90-nf', 'desc': '1-90_1567177197932.xml'},
                                 {'name': '180-nf', 'desc': '1-180_1567605482056.xml'},
                                 {'name': '360-nf', 'desc': '1-360_1567605514736.xml'},
                                 {'name': '540-nf', 'desc': '1-540_1567605552161.xml'},
                                 {'name': '700-nf', 'desc': '1-700_1567592027394.xml'},
                                 {'name': '1000-nf','desc': '1-1000_1568033962922.xml'}
                                ]
                   },
                   {'name': '12', 'address': tenant_addr, 'port': tenant_port, 'slice_desc': 'slice_info_12.yml',
                    'services': [{'name': '10-nf', 'desc': '1-10_1567427959592.xml'},
                                 {'name': '90-nf', 'desc': '1-90_1567177197932.xml'},
                                 {'name': '180-nf', 'desc': '1-180_1567605482056.xml'},
                                 {'name': '360-nf', 'desc': '1-360_1567605514736.xml'},
                                 {'name': '540-nf', 'desc': '1-540_1567605552161.xml'},
                                 {'name': '700-nf', 'desc': '1-700_1567592027394.xml'},
                                 {'name': '1000-nf','desc': '1-1000_1568033962922.xml'},
                                 {'name': '2000-nf','desc': '1-2000_1569232782532.xml'}
                                ]
                   }
                  ]
   
   print('### create slice 1 ###')
   slice1 = SliceExperiment('05', 'localhost', 5001, 'slice_descriptors/slice_info_5_a.yml')
   slice1.create_slice()

   raw_input('### create slice 2 ###')
   slice2 = SliceExperiment('05', 'localhost', 5001, 'slice_descriptors/slice_info_5_b.yml')
   slice2.create_slice()
   

   raw_input('### deploy service on slice 1 ###')
   slice1.submit_service('90-nf', 'service_descriptors/edit_config_create_1-90_1567177197932.xml')
   #slice1.submit_service('10-nf', 'service_descriptors/edit_config_create_1-10_1567427959592.xml')

   raw_input('### delete service on slice 1 ###')
   slice1.delete_service('90-nf', 'service_descriptors/edit_config_delete_1-90_1567177197932.xml') 
   #slice1.delete_service('10-nf', 'service_descriptors/edit_config_delete_1-10_1567427959592.xml')

   raw_input('### delete slices ###')
   slice1.delete_slice()
   slice2.delete_slice()
