set terminal pdfcairo
set output 'results/max_service_size.pdf'
set grid
set yrange[0:2500]
set xrange[1:12] 
set key font ",8"
set key spacing 0.75
set title 'Max Service Size'
set xlabel 'Number of resources (servers)'
set ylabel 'Max Number of Service Elements'

s=180
f(x)=s*x
plot f(x) notitle with lines
