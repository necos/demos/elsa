set terminal pdfcairo
set output 'results/max_service_size_per_slice.pdf'
set grid

set xrange[12:0]
set yrange[12:0]
set key font ",8"
set key spacing 0.75
set title 'Max Service Size'

set ylabel 'Servers' offset 2
set xlabel 'Slices'
set zlabel 'Service Elements' rotate by 90 

set ztics 250

set hidden3d
set ticslevel 0
set isosamples 50,50
set view 60, 200, 1, 1.5

f(x,y) = (x <= y) ? 180*y/x : 0

splot [1:12] [1:12] f(x,y) with lines title 'Service Elements per Slice'
