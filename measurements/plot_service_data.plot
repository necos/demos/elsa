set terminal pdfcairo
set output 'results/service-create-01.pdf'
set grid
set title 'Service Instantiation (Embedding)'
set xlabel 'number of service elements'
set ylabel 'time (s)'
plot 'results/service-orch-create-01.dat' u 1:2 w lp t 'Embedding'
set title 'Service Instantiation (Deployment)'
plot 'results/service-adapt-create-01.dat' u 1:2 w lp t 'Deployment'

set terminal pdfcairo
set output 'results/service-create-02.pdf'
set grid
set title 'Service Instantiation (Embedding)'
set xlabel 'number of service elements'
set ylabel 'time (s)'
plot 'results/service-orch-create-02.dat' u 1:2 w lp t 'Embedding'
set title 'Service Instantiation (Deployment)'
plot 'results/service-adapt-create-02.dat' u 1:2 w lp t 'Deployment'

set terminal pdfcairo
set output 'results/service-create-03.pdf'
set grid
set title 'Service Instantiation (Embedding)'
set xlabel 'number of service elements'
set ylabel 'time (s)'
plot 'results/service-orch-create-03.dat' u 1:2 w lp t 'Embedding'
set title 'Service Instantiation (Deployment)'
plot 'results/service-adapt-create-03.dat' u 1:2 w lp t 'Deployment'

set terminal pdfcairo
set output 'results/service-create-06.pdf'
set grid
set title 'Service Instantiation (Embedding)'
set xlabel 'number of service elements'
set ylabel 'time (s)'
plot 'results/service-orch-create-06.dat' u 1:2 w lp t 'Embedding'
set title 'Service Instantiation (Deployment)'
plot 'results/service-adapt-create-06.dat' u 1:2 w lp t 'Deployment'

set terminal pdfcairo
set output 'results/service-create-08.pdf'
set grid
set title 'Service Instantiation (Embedding)'
set xlabel 'number of service elements'
set ylabel 'time (s)'
plot 'results/service-orch-create-08.dat' u 1:2 w lp t 'Embedding'
set title 'Service Instantiation (Deployment)'
plot 'results/service-adapt-create-08.dat' u 1:2 w lp t 'Deployment'

set terminal pdfcairo
set output 'results/service-create-12.pdf'
set grid
set title 'Service Instantiation (Embedding)'
set xlabel 'number of service elements'
set ylabel 'time (s)'
plot 'results/service-orch-create-12.dat' u 1:2 w lp t 'Embedding'
set title 'Service Instantiation (Deployment)'
plot 'results/service-adapt-create-12.dat' u 1:2 w lp t 'Deployment'
