set terminal pdfcairo
set output 'results/monitoring.pdf'
set grid
set yrange[0:5000] 
set key font ",8"
set key spacing 0.75
set title 'IMA operations'
set xlabel 'number of resources (servers)'
set ylabel 'time (ms)'
plot 'results/slice-create.dat' u 1:5:9 w yerrorlines t 'Start Monitoring', 'results/slice-delete.dat' u 1:5:9 w yerrorlines t 'Delete Monitoring'


