from fabric import Connection

class SlicePartAbstraction:
      def __init__(self, host, name, port, vim_addr, vim_port):
          self.params = dict()
          self.params['host'] = host
          self.params['port'] = str(port)
          self.params['name'] = name
          self.params['vim_addr'] = vim_addr
          self.params['vim_port'] = str(vim_port)
          self.connection  = Connection(self.params['host'])

      def start(self):
          if 'washington' in self.params['host']:
             command = 'screen -dmS ' + 'do-' + self.params['name'] + ' scl enable python27 "python ~/vlsp-orchestrator/unify.py --gc ' + \
                       self.params['vim_addr'] + ':' + self.params['vim_port'] + ' --port ' + self.params['port'] + '"'
          else:
             command = 'screen -dmS ' + 'do-' + self.params['name'] + ' ~/vlsp-orchestrator/unify.py --gc ' + self.params['vim_addr'] + ':' + self.params['vim_port'] + ' --port ' + self.params['port']
 
          result = self.connection.run(command)
          if result.ok == True:
             command = 'screen -list | grep ' + self.params['name']
             result = self.connection.run(command, hide=True)
             self.pid = result.stdout.strip().split('.')[0].strip()
             print 'DO ' + self.params['name']  + ' successfully started => PID: ' + self.pid 
                         
      def stop(self):
          command = 'kill ' + self.pid
          result = self.connection.run(command)
          if result.ok == True:
             print 'DO ' + self.params['name'] + ' successfully shut down'

      def get_host(self):
          return self.params['host']

 
      def get_port(self):
          return self.params['port']


      def get_name(self):
          return self.params['name']
                    

if __name__ == '__main__':
   do1 = SlicePartAbstraction('washington.ee.ucl.ac.uk', 'edu', '8887', 'claytwo', '8888') 
   do1.start()
   raw_input("Press Enter to continue...")
   do1.stop()
