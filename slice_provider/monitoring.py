import requests, threading

class Monitoring:
      def __init__(self, controller_host, controller_port, aggregator_host, aggregator_port, slice_id):
          self.controller_host = controller_host
          self.controller_port = controller_port
          self.aggregator_host = aggregator_host
          self.aggregator_port = aggregator_port
          self.slice_id = slice_id

          self.abstractions = dict()

          self.controller_uri = 'http://' + self.controller_host + ':' + self.controller_port

          # using static values here
          self.control_port = '5555'
          self.info_port = '6699'
          self.ssh_port = '22'
          self.ssh_user = 'uceeftu'
          self.reporter_class = 'mon.lattice.appl.reporters.vlsp.LoggerReporter'
          self.reporter_name = 'logger-reporter'

 
      def resolve_probe_type(self, vim_type):
          if vim_type == 'vlsp':
             return 'mon.lattice.appl.probes.vlsp.VLSPProbe'

      def start_aggregator(self):
          url = self.controller_uri + '/dataconsumer/?port=' + self.ssh_port + '&username=' + self.ssh_user + '&endpoint=' + self.aggregator_host + '&args=' + \
                self.aggregator_port + '+' + self.controller_host + '+' + self.info_port + '+' + self.control_port

          r = requests.post(url, data=None).json()
          print r
          self.aggregator_id = r['ID'] 

          url = self.controller_uri + '/dataconsumer/' + self.aggregator_id + '/reporter/?className=' + self.reporter_class + '&args=' + self.reporter_name
          r = requests.post(url, data=None).json()
          self.reporter_id = r['createdReporterID']

          print self.aggregator_id
          print self.reporter_id

          #TODO: use parameters
          url = self.controller_uri + '/dataconsumer/' + self.aggregator_id + '/reporter/?className=mon.lattice.appl.reporters.vlsp.InfluxDBReporter' + '&args=influxDB+localhost+8086+slices'
          r = requests.post(url, data=None).json()
          self.reporter_id = r['createdReporterID']

          print self.reporter_id

    
      def add_abstraction(self, vim_type, vim_host, vim_port, slice_part_name, slice_part_id):
          self.abstractions[slice_part_name] = dict()
          self.abstractions[slice_part_name]['type'] = vim_type
          self.abstractions[slice_part_name]['vim_host'] = vim_host
          self.abstractions[slice_part_name]['vim_port'] = vim_port
          self.abstractions[slice_part_name]['name'] = slice_part_name
          self.abstractions[slice_part_name]['part_id'] = slice_part_id
 
          url = self.controller_uri + '/datasource/?port=' + self.ssh_port + '&username=' + self.ssh_user + '&endpoint=' + self.abstractions[slice_part_name]['vim_host'] + '&args=' + \
                self.aggregator_host + '+' + self.aggregator_port + '+' + self.controller_host + '+' + self.info_port + '+' + self.control_port

          r = requests.post(url, data=None).json()
          print r

          self.abstractions[slice_part_name]['ds_id'] = r['ID']


 
      def start_collecting(self):
          threads = []
          for slice_part_name in self.abstractions.iterkeys():
              t = threading.Thread(target=self.__start_probe, args=(slice_part_name, ))
              threads.append(t)
              t.start()

          for t in threads:
              t.join()



      def __start_probe(self, slice_part_name):
          try: 
              print self.abstractions[slice_part_name]['part_id']

              url = self.controller_uri + '/datasource/' + self.abstractions[slice_part_name]['ds_id'] + '/probe/?className=' + \
                    self.resolve_probe_type(self.abstractions[slice_part_name]['type']) + '&args=' + self.abstractions[slice_part_name]['vim_host'] + '+' + \
                    self.abstractions[slice_part_name]['vim_port'] + '+' + 'slice-part-abstraction' + '+' + self.abstractions[slice_part_name]['part_id']

              r = requests.post(url, data=None).json()
              print r
              self.abstractions[slice_part_name]['probe_id'] = r['createdProbeID']

              url = self.controller_uri + '/probe/' + self.abstractions[slice_part_name]['probe_id'] + '/?sliceid=' + self.slice_id
              r = requests.put(url, data=None).json()
              print r

              url = self.controller_uri + '/probe/' + self.abstractions[slice_part_name]['probe_id'] + '/?status=on'
              r = requests.put(url, data=None).json()

          except Exception as e:
              print e
         



      def stop_aggregator(self):
          url = self.controller_uri + '/dataconsumer/' + self.aggregator_id
          r = requests.delete(url).json()
          print r


 
      def remove_abstractions(self):
          threads = []
          for slice_part_name in self.abstractions.iterkeys():
              t = threading.Thread(target=self.__stop_probe, args=(slice_part_name, ))
              threads.append(t)
              t.start()

          for t in threads:
              t.join()
      

  
      def __stop_probe(self, slice_part_name):
          try:
              url = self.controller_uri + '/probe/' + self.abstractions[slice_part_name]['probe_id']
              r = requests.delete(url).json()
              print r
          except Exception as e:
              print e

          url = self.controller_uri + '/datasource/' + self.abstractions[slice_part_name]['ds_id']
          r = requests.delete(url).json()
          print r
