from slice_part import SlicePart
from abstractions import SlicePartAbstraction
from network import SlicePartNetwork
from monitoring import Monitoring
import time
import yaml
import pprint
import copy
import traceback
import uuid
import json
import threading

class Orchestrator:
      def __init__(self, slice_info_yml):
          self.slice_parts = dict()
          self.slice_connections = dict()
          self.slice_adapters = dict()
          self.slice_parts_chain = []
          self.slice_info_yml = slice_info_yml

      @classmethod
      def from_file(cls, slice_info_file):
          slice_info_ref = open(slice_info_file, 'r')
          return cls(slice_info_yml=slice_info_ref)




      def parse_slice_info(self):
          try: 
              slice_info = yaml.safe_load(self.slice_info_yml)
              p_slice_info = dict()

              #p_slice_info['id'] = slice_info['slices']['sliced']['id']
              p_slice_info['id'] = str(uuid.uuid4())
              p_slice_info['name'] = slice_info['slices']['sliced']['name'] 
              p_slice_info['dc'] = dict()
              p_slice_info['net'] = []
              p_slice_info['monitoring'] = dict()

              p_slice_info['monitoring']['engine'] = dict()
              p_slice_info['monitoring']['engine']['host'] = slice_info['slices']['sliced']['slice-monitoring']['engine']['ip']
              p_slice_info['monitoring']['engine']['port'] = str(slice_info['slices']['sliced']['slice-monitoring']['engine']['port'])

              p_slice_info['monitoring']['aggregator'] = dict()
              p_slice_info['monitoring']['aggregator']['host'] = slice_info['slices']['sliced']['slice-monitoring']['aggregator']['ip']
              p_slice_info['monitoring']['aggregator']['port'] = str(slice_info['slices']['sliced']['slice-monitoring']['aggregator']['port'])

              for info in slice_info['slices']['sliced']['slice-parts']:
                  if info.iterkeys().next() == 'dc-slice-part':
                     slice_part_info = dict()
                     
                     slice_part_info['controller'] = dict()
                     slice_part_info['controller']['host'] = info['dc-slice-part']['dc-slice-controller']['ip']
                     slice_part_info['controller']['port'] = str(info['dc-slice-part']['dc-slice-controller']['port'])

                     slice_part_info['abstraction'] = dict()
                     slice_part_info['abstraction']['host'] = info['dc-slice-part']['dc-slice-abstraction']['ip']
                     slice_part_info['abstraction']['port'] = str(info['dc-slice-part']['dc-slice-abstraction']['port'])

                     slice_part_info['vim'] = dict()
                     slice_part_info['vim']['type'] = info['dc-slice-part']['VIM']['type']
                     slice_part_info['vim']['size'] = str(info['dc-slice-part']['VIM']['host-count'])

                     p_slice_info['dc'][info['dc-slice-part']['name']] = slice_part_info
   
                  elif info.iterkeys().next() == 'net-slice-part':
                       net_slice_part_info = dict()
                       net_slice_part_info['name'] = info['net-slice-part']['name']
                       net_slice_part_info['id'] = info['net-slice-part']['net-slice-part-id']
                       net_slice_part_info['source'] = info['net-slice-part']['link']['source']
                       net_slice_part_info['destination'] = info['net-slice-part']['link']['destination']
          
                       p_slice_info['net'].append(net_slice_part_info)

                  elif info.iterkeys().next() == 'attachment-points':
                       attachment_points_info = dict()
                       attachment_points_info['ingress'] = dict()
                       attachment_points_info['egress'] = dict()
                       attachment_points_info['ingress']['name'] = info['attachment-points']['ingress']['name']
                       attachment_points_info['ingress']['dc-slice-part'] = info['attachment-points']['ingress']['dc-slice-part']
                       attachment_points_info['ingress']['port'] = str(info['attachment-points']['ingress']['port'])

                       attachment_points_info['egress']['name'] = info['attachment-points']['egress']['name']
                       attachment_points_info['egress']['dc-slice-part'] = info['attachment-points']['egress']['dc-slice-part']
                       attachment_points_info['egress']['host'] = info['attachment-points']['egress']['host']
                       attachment_points_info['egress']['port'] = str(info['attachment-points']['egress']['port'])

                       p_slice_info['attachment_points'] = attachment_points_info

              self.slice_info = p_slice_info

              return self.slice_info

          except yaml.YAMLError as exc:
              print(exc)




      def build_slice_parts_chain(self):
          current = self.slice_info['attachment_points']['ingress']['dc-slice-part']
          end = self.slice_info['attachment_points']['egress']['dc-slice-part']
          self.slice_parts_chain.append(current)
          net_slice_parts = copy.deepcopy(self.slice_info['net']) 
      
          while (current != end):
                prev_current = current
                for slice_part in net_slice_parts:
                    if slice_part['source'] == current:
                       current = slice_part['destination']
                       self.slice_parts_chain.append(current)
                       net_slice_parts.remove(slice_part)
                if prev_current == current:
                   print 'Error: cannot find next link'
                   break

          print self.slice_parts_chain




      def create_slice_parts(self):
          threads = []
          for part_name in self.slice_parts_chain:
              part = self.slice_info['dc'][part_name]
              self.slice_parts[part_name] = SlicePart(part['controller']['host'], part['controller']['port'], part_name, part['vim']['type'], part['vim']['size'])
              t = threading.Thread(target=self.slice_parts[part_name].create)
              threads.append(t)
              t.start()

          for t in threads:
              t.join()



          
      def start_abstractions(self):
          try:
              threads = []
              for part_name in self.slice_parts_chain:
                  part = self.slice_info['dc'][part_name]
                  self.slice_adapters[part_name] = SlicePartAbstraction(part['abstraction']['host'], 
                                                                        part_name, 
                                                                        part['abstraction']['port'], 
                                                                        self.slice_parts[part_name].get_vim_host(), 
                                                                        self.slice_parts[part_name].get_vim_port())
                  t = threading.Thread(target=self.slice_adapters[part_name].start)
                  threads.append(t)
                  t.start()

              for t in threads:
                  t.join()

          except:
              print 'Error while creating abstractions'



 
      def setup_connections(self):
          #creating connections from the egress to the ingress
          self.slice_parts_chain.reverse()

          next_ingress_host = self.slice_info['attachment_points']['egress']['host']
          next_ingress_port = self.slice_info['attachment_points']['egress']['port']
          current_egress = self.slice_info['attachment_points']['egress']['name']
          ingress_port = '8855'
          egress_port = '4000'

          for slice_part in self.slice_parts_chain:
  
              if slice_part != self.slice_info['attachment_points']['ingress']['dc-slice-part']:
                 current_ingress = 'link' + slice_part
              else:
                 current_ingress = self.slice_info['attachment_points']['ingress']['name']
                 
              self.slice_connections[slice_part] = SlicePartNetwork(self.slice_info['dc'][slice_part]['abstraction']['host'], 
                                                                    self.slice_info['dc'][slice_part]['abstraction']['port'], 
                                                                    current_ingress,
                                                                    current_egress)

              self.slice_connections[slice_part].add_egress(egress_port, 
                                                            next_ingress_host, 
                                                            next_ingress_port)

              self.slice_connections[slice_part].add_ingress(ingress_port, current_egress)
              self.slice_connections[slice_part].create_network()

              next_ingress_host = self.slice_connections[slice_part].get_mapping()[current_ingress]
              next_ingress_port = ingress_port
              current_egress = current_ingress




      def start_monitoring(self):
          threads = []          
          self.monitoring = Monitoring(self.slice_info['monitoring']['engine']['host'],
                                  self.slice_info['monitoring']['engine']['port'],
                                  self.slice_info['monitoring']['aggregator']['host'],
                                  self.slice_info['monitoring']['aggregator']['port'],
                                  self.slice_info['id'])

          t = threading.Thread(target=self.monitoring.start_aggregator)
          threads.append(t)
          t.start()

          for part_name in self.slice_parts.iterkeys():
              t = threading.Thread(target=self.monitoring.add_abstraction, args=(self.slice_parts[part_name].get_vim_type(),
                                                                                 self.slice_parts[part_name].get_vim_host(),
                                                                                 self.slice_parts[part_name].get_vim_port(),
                                                                                 part_name,
                                                                                 self.slice_parts[part_name].get_slice_id(), ))
              threads.append(t)
              t.start()

          for t in threads:
              t.join()
          
          #multiple threads are created internally      
          self.monitoring.start_collecting()
              



      def stop_monitoring(self):
          self.monitoring.stop_aggregator()

          #multiple threads are created internally
          self.monitoring.remove_abstractions()




      def get_slice_entrypoint(self):
          try: 
             #we always use a standard template for this
             slice_config_template = open('slice_config_template.json', 'r')
             self.slice_config = json.load(slice_config_template)
             adaptation = self.slice_config['adaptation']
             managers = adaptation['MANAGERS']

             for slice_adapter_name in self.slice_adapters.iterkeys():
                 do_info = dict()
                 do_info['module'] = 'escape.adapt.managers'
                 do_info['class'] = 'UnifyDomainManager'
                 do_info['diff'] = True
                 do_info['poll'] = False
                 do_info['adapters'] = {}
                 do_info['adapters']['REMOTE'] = {}
                 do_info['adapters']['REMOTE']['timeout'] = 20
                 do_info['adapters']['REMOTE']['module']  = 'escape.adapt.adapters'
                 do_info['adapters']['REMOTE']['class'] = 'UnifyRESTAdapter'
                 do_info['adapters']['REMOTE']['prefix'] = 'ro/infra' 
                 do_info['domain_name'] = self.slice_adapters[slice_adapter_name].get_name()
                 do_info['adapters']['REMOTE']['url'] = 'http://' + self.slice_adapters[slice_adapter_name].get_host() + ':' + self.slice_adapters[slice_adapter_name].get_port()
                 adaptation[slice_adapter_name] = do_info             
  
                 managers.append(slice_adapter_name)

             adaptation['MANAGERS'] = managers
             self.slice_config['adaptation'] = adaptation
             return self.slice_config

          except Exception as ex:
             print ex
             traceback.print_exc()




      def remove_connections(self):
          try:
              threads = []
              for part_name in self.slice_parts_chain:
                  t = threading.Thread(target=self.slice_connections[part_name].delete_network)
                  threads.append(t)
                  t.start()

              for t in threads:
                  t.join()

          except Exception as e:
              print 'Error while removing connections'
              print e



  
      def delete_slice_parts(self):
          threads = []
          for part_name in self.slice_parts_chain:
              t = threading.Thread(target=self.slice_parts[part_name].delete)
              threads.append(t)
              t.start()

          for t in threads:
                  t.join()




      def stop_abstractions(self):
          threads = []
          for part_name in self.slice_parts_chain:
              t = threading.Thread(target=self.slice_adapters[part_name].stop)
              threads.append(t)
              t.start()

          for t in threads:
              t.join()




      def get_slice_id(self):
          return self.slice_info['id']


      def get_slice_parts(self):
          return self.slice_parts_chain




if __name__ == '__main__':
   pp = pprint.PrettyPrinter(indent=2)
   orchestrator = Orchestrator.from_file('slice_info.yml')
   pp.pprint(orchestrator.parse_slice_info())

   try:
      orchestrator.build_slice_parts_chain() 
      orchestrator.create_slice_parts()
      orchestrator.start_abstractions()
 
      #waiting for the abstractions to come up
      time.sleep(3) 
    
      orchestrator.setup_connections()
      #pp.pprint(orchestrator.start_monitoring())

      pp.pprint(orchestrator.get_slice_entrypoint())


   except Exception as ex:
      print ex
      traceback.print_exc()

   finally:
     raw_input("Press a key to shutdown the slice...")

     #orchestrator.stop_monitoring()
     orchestrator.remove_connections()
     orchestrator.stop_abstractions()
     orchestrator.delete_slice_parts()
     
