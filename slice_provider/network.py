import xml.etree.ElementTree as ET
import requests
import ast

class SlicePartNetwork:

      def __init__(self, address, port, ingress, egress):
          self.address = address
          self.port = port
          self.ingress = ingress
          self.egress = egress

          self.virtualizer = ET.Element('virtualizer')
          ET.SubElement(self.virtualizer, 'id').text = 'infra'
          nodes = ET.SubElement(self.virtualizer, 'nodes')
          node = ET.SubElement(nodes, 'node')
          ET.SubElement(node, 'id').text = 'infra'          
          self.ports = ET.SubElement(node, 'ports')

          self.virtualizer_delete = ET.Element('virtualizer')
          ET.SubElement(self.virtualizer_delete, 'id').text = 'infra'
          nodes_delete = ET.SubElement(self.virtualizer_delete, 'nodes')
          node_delete = ET.SubElement(nodes_delete, 'node')
          ET.SubElement(node_delete, 'id').text = 'infra'          
          self.ports_delete = ET.SubElement(node_delete, 'ports')

                      

      def add_ingress(self, local_port, destination):
          port = ET.SubElement(self.ports, 'port')
          port.set('operation', 'create')
          ET.SubElement(port, 'id').text = self.ingress
          ET.SubElement(port, 'name').text = self.ingress
          ET.SubElement(port, 'port_type').text = 'port-sap'
          addresses = ET.SubElement(port, 'addresses')

          lfour = dict()
          lfour['type'] = 'ingress'
          lfour['local'] = local_port
          lfour['destination'] = destination

          ET.SubElement(addresses, 'l4').text = str(lfour)        
          

      def add_egress(self, local_port, destination_host, destination_port):
          port = ET.SubElement(self.ports, 'port')
          port.set('operation', 'create')
          ET.SubElement(port, 'id').text = self.egress
          ET.SubElement(port, 'name').text = self.egress          
          ET.SubElement(port, 'port_type').text = 'port-sap'
          addresses = ET.SubElement(port, 'addresses')
          
          lfour = dict()
          lfour['type'] = 'egress'
          lfour['local'] = local_port
          lfour['destination'] = destination_host + ':' + destination_port

          ET.SubElement(addresses, 'l4').text = str(lfour)

      def get_create_XML(self):
          return ET.tostring(self.virtualizer)

      def get_delete_XML(self): 
          return ET.tostring(self.virtualizer_delete)

      def get_mapping(self):
          url = 'http://' + self.address + ':' + self.port + '/ro/infra/get-config?blocking'
          r = requests.get(url)
          mapping = dict()
          if r.status_code == requests.codes.ok:
             root = ET.fromstring(r.text)
             for port in root.iter('port'):
                 port_id = port.find('id').text
                 address_data = ast.literal_eval(port.find('addresses').find('l4').text)
                 if port_id == self.ingress or port_id == self.egress:
                    mapping[port_id] = address_data['host']
          return mapping


      def create_network(self):
          url = 'http://' + self.address + ':' + self.port + '/ro/infra/edit-config?blocking'
          print 'Creating network on: ' + url
          r = requests.post(url, data=self.get_create_XML(), headers={'Content-Type': 'application/xml'})
          if r.status_code == requests.codes.ok:
             print 'Network successfully setup'

      def delete_ingress_egress(self):
          iport = ET.SubElement(self.ports_delete, 'port')
          iport.set('operation', 'delete')
          ET.SubElement(iport, 'id').text = self.ingress
          ET.SubElement(iport, 'name').text = self.ingress
          ET.SubElement(iport, 'port_type').text = 'port-sap'          

          eport = ET.SubElement(self.ports_delete, 'port')
          eport.set('operation', 'delete')
          ET.SubElement(eport, 'id').text = self.egress
          ET.SubElement(eport, 'name').text = self.egress
          ET.SubElement(eport, 'port_type').text = 'port-sap'          


      def delete_network(self):
          self.delete_ingress_egress()
          url = 'http://' + self.address + ':' + self.port + '/ro/infra/edit-config?blocking'
          print 'Deleting network on: ' + url
          r = requests.post(url, data=self.get_delete_XML(), headers={'Content-Type': 'application/xml'})
          if r.status_code == requests.codes.ok:
             print 'Network successfully deleted'
