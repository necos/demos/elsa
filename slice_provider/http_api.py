from flask import Flask, jsonify, request, make_response, json
from orchestrator import Orchestrator
import time
import traceback

app = Flask(__name__)

slices = dict()

#curl --data-binary @slice_info.yml -X POST -H "Content-Type: text/x-yaml" http://localhost:5000/orchestrator/slice
@app.route('/orchestrator/slice', methods=['POST'])
def create_slice():
    try:
        t_start = int(round(time.time() * 1000))

        orchestrator = Orchestrator(request.data)
        t_startup = int(round(time.time() * 1000))

        print '### Parsing Slice Descriptor ###'
        orchestrator.parse_slice_info()
        t_proc = int(round(time.time() * 1000))

        orchestrator.build_slice_parts_chain()
        t_create_chain = int(round(time.time() * 1000))

        print '\n### Creating Slice Parts ###'
        raw_input()
        orchestrator.create_slice_parts()
        t_create_parts = int(round(time.time() * 1000))

        print '\n### Creating Slice Parts Abstractions ###'
        raw_input()
        orchestrator.start_abstractions()
        t_create_abstractions = int(round(time.time() * 1000))

        time.sleep(2)

        print '\n### Creating Network Connections between the Slice Parts ###'
        raw_input()
        orchestrator.setup_connections()
        t_setup_connections = int(round(time.time() * 1000))

        print '\n### Deploying Monitoring Abstractions and Aggregator ###'
        raw_input()
        orchestrator.start_monitoring()
        t_monitoring = int(round(time.time() * 1000))

        slice_id = orchestrator.get_slice_id()
        slice_parts = orchestrator.get_slice_parts()
        slices[slice_id] = orchestrator
        print slice_id

        stats = dict()        
        stats['startup'] = t_startup - t_start
        stats['proc'] = t_proc - t_startup
        stats['chain'] = t_create_chain - t_proc
        stats['parts'] = t_create_parts - t_create_chain
        stats['abstractions'] = t_create_abstractions - t_create_parts
        stats['connections'] = t_setup_connections - t_create_abstractions - 2000
        stats['monitoring'] = t_monitoring - t_setup_connections

        f_stats = open('stats/stats-create-' + slice_id, 'w')
        json.dump(stats, f_stats)
        f_stats.close()
         
        return jsonify({'slice_id' : slice_id, 'slice_parts' : slice_parts})
    except Exception as ex:
        print ex
        traceback.print_exc()
        return make_response(jsonify({'error': 'Internal Server Error'}), 500)


@app.route('/orchestrator/slice/<string:slice_id>', methods=['DELETE'])
def delete_slice(slice_id):
    try: 
        orchestrator = slices[slice_id]

        t_start = int(round(time.time() * 1000))
        
        orchestrator.stop_monitoring()
        t_monitoring = int(round(time.time() * 1000))

        orchestrator.remove_connections()
        t_delete_connections = int(round(time.time() * 1000))

        orchestrator.stop_abstractions()
        t_remove_abstractions = int(round(time.time() * 1000))

        time.sleep(2)

        orchestrator.delete_slice_parts()
        t_delete_parts = int(round(time.time() * 1000))

        stats = dict()
        stats['monitoring'] = t_monitoring - t_start
        stats['connections'] = t_delete_connections - t_monitoring
        stats['abstractions'] = t_remove_abstractions - t_delete_connections
        stats['parts'] = t_delete_parts - t_remove_abstractions - 2000

        f_stats = open('stats/stats-delete-' + slice_id, 'w')
        json.dump(stats, f_stats)
        f_stats.close()
 
        return jsonify({'result' : True})
    except:
        make_response(jsonify({'error': 'Internal Server Error'}), 500)
        

@app.route('/orchestrator/slice/<string:slice_id>', methods=['GET'])
def get_slice_entrypoint(slice_id):
    try:
        orchestrator = slices[slice_id]
        return jsonify({'entry_point' : orchestrator.get_slice_entrypoint()})
    except:
        return make_response(jsonify({'error': 'Internal Server Error'}), 500)


@app.route('/orchestrator/slice/', methods=['GET'])
def list_slices():
    try:
        print slices.keys()
        return jsonify({'slices': slices.keys()})
    except:
        return make_response(jsonify({'error': 'Internal Server Error'}), 500)



if __name__ == '__main__':
    app.run(debug=True)
