import requests


class SlicePart:
      def __init__(self, controller_addr, controller_port, name, slice_type, slice_size):
          self.params = dict()
          self.params['controller_addr'] = controller_addr
          self.params['controller_port'] = controller_port
          self.params['name'] = name
          self.params['slice_type'] = slice_type
          self.params['slice_size'] = slice_size
	  self.url = 'http://' + self.params['controller_addr'] + ':' + self.params['controller_port'] + '/slice/'

      def create(self):
	  slice_data = {"type" : self.params['slice_type'], "size" : self.params['slice_size']}
	  
	  try:
             create_url = self.url
	     print 'Requesting creation of slice ' + str(slice_data) + ' to the Slice Controller - url: ' + create_url

             r = requests.post(create_url, json=slice_data).json()
             print r
	     self.vim_hostname = r['payload']['vim']['hostname']
	     self.vim_port = str(r['payload']['vim']['port'])
             self.slice_id = str(r['payload']['id'])
             print 'Created Slice ' + self.slice_id + ' => Slice 0 on ' + self.vim_hostname + ':' + self.vim_port
 
          except Exception as e:
             print 'Cannot get a slice reference from the Slice Controller - url: ' + self.url
             print e
          

      def delete(self):

          try:
             delete_url = self.url + str(self.slice_id)
             print 'Requesting deletion of slice ' + str(self.slice_id) + ' to the Slice Controller - url: ' + delete_url

             r = requests.delete(delete_url).json()
             print r
             if r['payload']['id'] == self.slice_id:
                print 'Deleted Slice ' + str(self.slice_id)

          except Exception as e:
             print 'Cannot delete slice from Slice Controller - url: ' + self.url
             print e


      def get_vim_host(self):
          return self.vim_hostname


      def get_vim_port(self):
          return self.vim_port


      def get_vim_type(self):
          return self.params['slice_type']


      def get_slice_id(self):
          return self.slice_id
