#!/bin/bash

pgrep -f viewer | xargs kill 2>&1

screen -dmS vis-clay-1 ~/startVis.sh claytwo 8888 8080
screen -dmS vis-gas-1 ~/startVis.sh methane 8888 8081
screen -dmS vis-edu-1 ~/startVis.sh washington 8888 8082

screen -dmS vis-clay-2 ~/startVis.sh claytwo 8889 9080
screen -dmS vis-gas-2 ~/startVis.sh methane 8889 9081
screen -dmS vis-edu-2 ~/startVis.sh washington 8889 9082
