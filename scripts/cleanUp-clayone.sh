#!/bin/bash

pkill -f dataconsumer

screen -list | grep do | cut -d '.' -f 1 | xargs kill &>/dev/null
