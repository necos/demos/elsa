#!/bin/bash

screen -S lattice -X quit 2>&1
screen -S slice -X quit 2>&1
sudo screen -S tenant -X quit 2>&1

screen -dmS lattice ~/NECOS/exp_scripts/startLattice.sh
screen -dmS slice ~/NECOS/exp_scripts/startSlice.sh
sudo screen -dmS tenant /home/uceeftu/NECOS/exp_scripts/startTenant.sh
