#!/bin/bash

containers=`docker ps -a | grep orch- | awk '{print $1}'`

if [ ! -z "$containers" ] 
   then
   echo $containers | xargs docker stop
   echo $containers | xargs docker rm
fi
