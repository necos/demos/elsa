The software components related to the Resource Provider are available from the following external repositories

 * **DC Slice Controller**: https://gitlab.com/necos/ucl-dc-slice-controller
 * **VLSP**: https://github.com/stuartclayman/VLSP  
